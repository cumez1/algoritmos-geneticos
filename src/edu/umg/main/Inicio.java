package edu.umg.main;

import de.javasoft.plaf.synthetica.SyntheticaAluOxideLookAndFeel;
import edu.umg.view.Principal;
import java.text.ParseException;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;


public class Inicio {
    public static void main(String[] args) {      
        
        try {
            UIManager.setLookAndFeel(new SyntheticaAluOxideLookAndFeel());
        } catch (ParseException | UnsupportedLookAndFeelException e) {
            System.out.println("Error "+e);
        }
        Principal princ = new Principal();
        princ.setVisible(true);
        princ.show();

  
    }   
    
}
