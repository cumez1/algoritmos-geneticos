/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.umg.ochoreinas;

/**
 *
 * @author Nicolás Cúmez
 */
public class CuadroAjedrez {

    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;
    
    public CuadroAjedrez (int[] row){
        this.setA(String.valueOf(row[0]));
        this.setB(String.valueOf(row[1]));
        this.setC(String.valueOf(row[2]));
        this.setD(String.valueOf(row[3]));
        this.setE(String.valueOf(row[4]));
        this.setF(String.valueOf(row[5]));
        this.setG(String.valueOf(row[6]));
        this.setH(String.valueOf(row[7]));

    }

    public String getA() {
        return this.a;
    }

    public void setA(String a) {
        if(a.equals("0"))this.a = "";else this.a = a;
    }

    public String getB() {
        return this.b;
    }

    public void setB(String b) {
        if(b.equals("0"))this.b = "";else this.b = b;
    }

    public String getC() {
        return this.c;
    }

    public void setC(String c) {
        if(c.equals("0"))this.c = "";else this.c = c;
    }

    public String getD() {
        return this.d;
    }

    public void setD(String d) {
        if(d.equals("0"))this.d = "";else this.d = d;
    }

    public String getE() {
        return this.e;
    }

    public void setE(String e) {
        if(e.equals("0"))this.e = "";else this.e = e;
    }

    public String getF() {
        return this.f;
    }

    public void setF(String f) {
        if(f.equals("0"))this.f = "";else this.f = f;
    }

    public String getG() {
        return this.g;
    }

    public void setG(String g) {
        if(g.equals("0"))this.g = "";else this.g = g;
    }

    public String getH() {
        return this.h;
    }

    public void setH(String h) {
        if(h.equals("0"))this.h = "";else this.h=h;
    }
}
